imgcache
========

`imgcache` is an [OpenResty](https://openresty.org/en/) application which caches and rate limits requests to any facial analysis API. See `conf/config.lua` for configuration options. While imgcache currently depends on [Redis](https://redis.io/), with trivial modifications you could use the [shared dictionary](https://github.com/openresty/lua-nginx-module#ngxshareddict) as the only backing store. I do not recommend this as the shared dictionary would reset whenever the nginx server quits.

Included in `ext` is an example [Chrome extension](https://developer.chrome.com/extensions) which removes images with the returned hashes from a certain popular image board. 

Why
===

Sometimes moderators have other things to do and bad content, which I'd rather not see, slips through. My personal deployment of imgcache hides images that the [Microsoft Computer Vision API](https://www.microsoft.com/cognitive-services/en-us/computer-vision-api) considers adult content and highlights the post so that I may report them. Microsoft limits API requests to three per second and this cache was a quick way to minimize the number of calls and store the results for each image hash in a more long term manner. In the future, I'd like to add some more workers which check other APIs.

