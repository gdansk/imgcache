-- rate limited api cacher

local delay = 3.01  -- in seconds
local new_timer = ngx.timer.at
local log = ngx.log
local ERR = ngx.ERR
local INFO = ngx.INFO
local settings = ngx.shared.settings
local hashes   = ngx.shared.hashes
local check
local check_api

check_api = function(hash, image, redis)
	local http  = require "resty.http"
	local json  = require "cjson"
	local config = require('config')

	local httpc = http.new()
	local body =json.encode{url=image}
	local res, err = httpc:request_uri(config.cvapi.url, {
		method  = "POST",
		headers = config.cvapi.headers(),
		body    = body,
	})
	log(INFO, "hash: ", hash)
	log(INFO, "image: ", image)
	log(INFO, "sent body: ", body)

	if not res then
		return nil, err
	end

	log(INFO, "response ", res.body)
	local res, err = json.decode(res.body)
	if res then
		return config.cvapi.check(res, redis, hashes, hash)
	else
		return nil, err
	end
end

local processing = "processing" .. tostring(ngx.worker.id())
check = function(premature)
	local wpid, err = settings:get("cvworker")  
	if wpid == nil then
		local suc, err = settings:set("cvworker", ngx.worker.pid())
		if not suc then 
			log(ERR, err) 
		else 
			wpid = ngx.worker.pid()
		end
	end
	if wpid ~= ngx.worker.pid() then
		log(ERR, "worker wrong id", tostring(ngx.worker.pid()), settings:get("cvworker"))
		return
	end
	if not premature then
		local redis = require('rediscon')()

		-- pop from work queue, push to processing queue
		local workitem = redis:rpoplpush("work", processing)
		if workitem ~= ngx.null then
			local regex = [=[(.+)@(.+)]=] -- hash:image.ext
			local m = ngx.re.match(workitem, regex)

			local hash, image = m[1], m[2]

			-- check to make sure it's not set, dont
			local stored = redis:get(hash)
			log(ERR, "workitem: ", workitem, " stored: ", stored)
			if redis:get(hash) == ngx.null then
				-- check the comp vision 
				local suc, err = check_api(hash, image, redis)
				if not suc then
					-- if API call failed, move to back of queue
					redis:lpush("work", workitem)
					log(ERR, err)
				end
			end
			-- remove from the processing queue
			redis:lrem(processing)
		end

		--restart timer
		local ok, err = new_timer(delay, check)
		if not ok then
			log(ERR, "failed to create timer: ", err)
			return
		end
	end
end

local ok, err = new_timer(0, check)
if not ok then
	log(ERR, "failed to create timer: ", err)
	return
end
