local config = require "config"
local redis = require "resty.redis"

local function getredis()
	local red = redis:new()

	red:set_timeout(1000) -- 1 sec

	local ok, err = red:connect(config.redis.url, config.redis.port)
	if not ok then
		ngx.say('{"error":"failed to connect: ' .. err .. '"}')
		return
	end
	local res, err = red:auth(config.redis.pass)
	if not res then
		ngx.say('{"error":"failed to authenticate: ' .. err .. '"}')
		return
	end

	return red
end

return getredis
