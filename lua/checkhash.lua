local json = require("cjson")
local redis = require("rediscon")()
local hashes = ngx.shared.hashes

local results = { hashes = {}, queue = 0 }

-- add image to the work queue
local function enqueue(image)
	redis:lpush("work", table.concat{image.hash, "@", image.url})
end

local function add(score, image)
	if(tonumber(score)==1) then 
		results.hashes[#results.hashes + 1] = image.hash
	end
end

ngx.req.read_body()
local body = ngx.req.get_body_data()
if body then body = json.decode(body) end

if body then
	for i,image in ipairs(body.images) do 
		local score = hashes:get(image.hash)
		ngx.log(ngx.ERR, "checking hash ", image.hash, " has score ", score)
		if score ~= nil then
			add(score, image)
		else 
			-- otherwise, enqueue the image for an API check
			score = redis:get(image.hash)
			if score ~= ngx.null then
				add(score, image)
			else 
				enqueue(image)
				results.queue = results.queue + 1
			end
		end
	end
end

ngx.say(json.encode(results));
