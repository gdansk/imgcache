-- CONFIG --
local keys = {
	"your-api-keys-go-here",
	"as-many-as-like"
}

local config = {
	redis = {
		url = "127.0.0.1", -- Your redis instance IP
		port = "6379",     -- redis port
		-- password for redis auth
		pass = "an-extremely-long-and-very-strong-password" 
	},
	cvapi = {
		-- URL of the API to call
		url = "https://api.projectoxford.ai/vision/v1.0/analyze?visualFeatures=Whatever&language=en",
		-- A function that handles the response
		-- Redis is the redis connection. For more see: openresty redis
		-- Hashes is a shared dictionary between all the workers
		-- Hash is the hash of the current work item
		check = function(response, redis, hashes, hash)
			-- Generally you look through the response to make sure it's valid
			-- If it meets a certain criteria (i.e. adult content) you can add it to the redis and hashmap like so
			redis:set(hash, 1)
			hashes:set(hash, 1)
			-- If it doesn't meet the criteria set it to 0:
			redis:set(hash, 0)
			hashes:set(hash, 0)
			-- Otherwise it will be readded to the work queue each time!
		end,
		headers = function()
			return {
				-- Add your API headers here
				["Content-Type"] = "application/json",
				["Ocp-Apim-Subscription-Key"] = keys[math.random(1,#keys)]
			}
		end,
	}
}

return config
