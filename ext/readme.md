A simple chrome extension to remove images
==========================================
Since most freely available image classification APIs are heavily rate limited this uses imcache to cache the API calls and to rate limit requests.

Improvements
============
* Inject the hashes into the users filter
* Re-run whenever a thread update request finishes

