var imgcache_URL = 'http://localhost'
var data = { images: [] };
var images = document.getElementsByClassName('fileThumb');
for(var i = images.length-1; i >= 0; i--){
    var url = images[i].getAttribute('href').replace('//', 'https://');
    var ext = url.split('.').pop();
    if(ext === "png" || ext === "jpg") {
        var hash = images[i].firstChild.getAttribute('data-md5').replace('==', '');
        data.images.push({url: url, hash: hash})
    }
}

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState != 4) return;
    var response = this.response;
    console.log(response);
    if (response === null || response.length < 1){
        return;
    }
    for(var i = 0; i < response.length; i++){
        for(var j = 0; j < images.length; j++){
            if(images[j].firstChild.getAttribute('data-md5') === (response[i]+'==')) {
                console.log(response[i])
                images[j].removeChild(images[j].firstChild);
            }
        }
    }
}

xhttp.responseType='json';
xhttp.open('POST', imgcache_URL, true);
xhttp.setRequestHeader('Content-Type', 'application/json');
xhttp.send(JSON.stringify(data));
